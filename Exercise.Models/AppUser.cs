﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercise.Models
{
    public class AppUser : IdentityUser<Guid>
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string RefreshToken { get; set; }

        public virtual ICollection<Elevator> Elevators { get; set; }
    }
}
