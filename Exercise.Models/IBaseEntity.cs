﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Exercise.Models
{
    public interface IBaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        Guid Id { get; set; }
        string CreateBy { get; set; }
        string LastUpdateBy { get; set; }
        bool IsDeleted { get; set; }
        DateTime? DeletedDate { get; set; }
        DateTime DateCreated { get; set; }
    }
}
