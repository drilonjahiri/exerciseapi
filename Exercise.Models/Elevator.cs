﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Exercise.Models
{
    public class Elevator : BaseEntity
    {
        [Timestamp]
        public byte[] LastUpdated { get; set; }

        public Guid? StatusId { get; set; }
        public virtual Status Status { get; set; }

        public Guid? UserId { get; set; }
        public virtual AppUser User { get; set; }
    }
}
