﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercise.Models
{
    public class UserRole : IdentityUserRole<Guid>
    {
        public virtual AppUser User { get; set; }
        public virtual Role Role { get; set; }
    }
}
