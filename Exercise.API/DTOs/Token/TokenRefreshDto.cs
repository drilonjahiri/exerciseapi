﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercise.API.DTOs.Token
{
    public class TokenRefreshDto
    {
        public string token { get; set; }
        public string refreshToken { get; set; }
    }
}
