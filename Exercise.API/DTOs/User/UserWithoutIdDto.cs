﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercise.API.DTOs.User
{
    public class UserWithoutIdDto
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
    }
}
