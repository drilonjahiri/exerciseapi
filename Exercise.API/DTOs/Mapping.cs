﻿using AutoMapper;
using Exercise.API.DTOs.Elevator;
using Exercise.API.DTOs.User;
using Exercise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercise.API.DTOs
{
    public class Mapping : Profile
    {
        
        public Mapping()
        {
            // User
            CreateMap<AppUser, UserReturnDto>().ReverseMap();
            CreateMap<AppUser, UserReturnDto>().ReverseMap();
            CreateMap<AppUser, UserWithoutIdDto>().ReverseMap();

            //elevator
            CreateMap<Exercise.Models.Elevator, ElevatorDto>()
                .ForMember(x => x.ElevatorId, opt => opt.MapFrom(src => src.Id))
                .ForMember(x => x.Status, opt => opt.MapFrom(src => src.Status.Name))
                .ForMember(x => x.LastUpdated, opt => opt.MapFrom(src => src.LastUpdatedDate));
        }
    }
}
