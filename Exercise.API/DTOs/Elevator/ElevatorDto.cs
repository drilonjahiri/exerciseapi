﻿using Exercise.API.DTOs.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Exercise.API.DTOs.Elevator
{
    public class ElevatorDto
    {
        public Guid ElevatorId { get; set; }
        public string Status { get; set; }
        public DateTime LastUpdated { get; set; }

        public UserWithoutIdDto User { get; set; }
    }
}
