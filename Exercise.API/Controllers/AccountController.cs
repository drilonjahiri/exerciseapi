﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Exercise.API.DTOs.User;
using Exercise.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Exercise.API.Helpers;
using System.Security.Claims;
using Exercise.API.DTOs.Token;

namespace Exercise.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly ITokenService _tokenService;
        private readonly IMapper mapper;
        private readonly IConfiguration _config;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;

        public AccountController(
                IMapper mapper,
                IConfiguration config,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager,
                ITokenService tokenService)
                {
                    this.mapper = mapper;
                    _config = config;
                    _userManager = userManager;
                    _signInManager = signInManager;
                    _tokenService = tokenService;
                }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login(UserLoginDto userLoginDto)
        {
            var user = await _userManager.FindByNameAsync(userLoginDto.UserName);
            if (user == null)
            {
                return Unauthorized();
            }

            var result = await _signInManager.CheckPasswordSignInAsync(user, userLoginDto.Password, false);

            if (result.Succeeded)
            {
                var appUser = await _userManager.Users
                    .FirstOrDefaultAsync(u => u.NormalizedUserName == userLoginDto.UserName.ToUpper());

                var userToReturn = mapper.Map<UserReturnDto>(appUser);

                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier,user.Id.ToString()),
                    new Claim(ClaimTypes.Name, user.UserName.ToString())
                };

                var roles = await _userManager.GetRolesAsync(user);
                foreach (var role in roles)
                {
                    claims.Add(new Claim(ClaimTypes.Role, role));
                }

                var jwtToken = _tokenService.GenerateAccessToken(claims);
                var refToken = _tokenService.GenerateRefreshToken();

                user.RefreshToken = refToken;
                await _userManager.UpdateAsync(user);

                return Ok(new
                {
                    accessToken = jwtToken,
                    refreshToken = refToken,
                    user = userToReturn
                });
            }
            return Unauthorized();
        }

        [AllowAnonymous]
        [HttpPost("refreshToken")]
        public async Task<IActionResult> Refresh(TokenRefreshDto model)
        {
            var principal = _tokenService.GetPrincipalFromExpiredToken(model.token);
            var username = principal.Identity.Name; //this is mapped to the Name claim by default

            var user = _userManager.Users.SingleOrDefault(u => u.UserName == username);
            if (user == null || user.RefreshToken != model.refreshToken) return BadRequest();

            var newJwtToken = _tokenService.GenerateAccessToken(principal.Claims);
            var newRefreshToken = _tokenService.GenerateRefreshToken();

            user.RefreshToken = newRefreshToken;
            await _userManager.UpdateAsync(user);

            return Ok(new
            {
                accessToken = newJwtToken,
                refreshToken = newRefreshToken
            });
        }

        [HttpPost("revoke")]
        public async Task<IActionResult> Revoke()
        {
            var username = User.Identity.Name;

            var user = _userManager.Users.SingleOrDefault(u => u.UserName == username);
            if (user == null) return BadRequest();

            user.RefreshToken = null;

            await _userManager.UpdateAsync(user);

            return NoContent();
        }
    }
}