﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Exercise.API.DTOs.Elevator;
using Exercise.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Exercise.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ElevatorController : ControllerBase
    {
        private readonly IMapper mapper;
        private readonly IElevatorService elevatorService;

        public ElevatorController(IMapper mapper, IElevatorService elevService)
        {            
            this.mapper = mapper;
            this.elevatorService = elevService;            
        }

        [HttpGet("GetAll")]
        public IActionResult GetAll()
        {
            var username = User.Identity.Name;
            var elevators = mapper.Map<IEnumerable<ElevatorDto>>(elevatorService.GetAllByAccount(username));
            return this.Ok(elevators);
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(Guid id)
        {
            ElevatorDto elevator = mapper.Map<ElevatorDto>(elevatorService.GetById(id));

            if (elevator == null)
            {
                return NotFound();
            }

            var username = User.Identity.Name;

            if (elevator.User?.Username != username)
            {
                return Unauthorized();
            }

            return this.Ok(elevator);
        }
    }
}