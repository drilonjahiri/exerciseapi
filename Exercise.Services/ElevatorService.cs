﻿using Exercise.Models;
using Exercise.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise.Services
{
    public class ElevatorService : IElevatorService
    {
        private readonly IUnitOfWork unitOfWork;

        public ElevatorService(IUnitOfWork unitOfwork)
        {
            unitOfWork = unitOfwork;
        }

        public bool Delete(Elevator item)
        {
            unitOfWork.Repository<Elevator>().Delete(item);
            this.SaveChanges();
            return true;
        }

        public IEnumerable<Elevator> GetAll()
        {
            return unitOfWork.Repository<Elevator>().Get();
        }

        public IEnumerable<Elevator> GetAllByAccount(string username)
        {
            return unitOfWork.Repository<Elevator>().Get(x => x.User.UserName == username);
        }

        public Elevator GetById(Guid Id)
        {
            return this.unitOfWork.Repository<Elevator>().GetById(Id);
        }

        public Elevator Insert(Elevator Item, string[] include = null)
        {
            var addedItem = unitOfWork.Repository<Elevator>().Insert(Item);
            this.SaveChanges();
            if (include != null)
            {
                addedItem = unitOfWork.Repository<Elevator>().Get(x => x.Id == addedItem.Id, null, include).FirstOrDefault();
            }
            return addedItem;
        }

        public bool InsertRange(List<Elevator> Items)
        {
            foreach (var item in Items)
            {
                unitOfWork.Repository<Elevator>().Insert(item);
            }
            this.SaveChanges();
            return true;
        }

        public bool Update(Elevator Item)
        {
            var updated = this.unitOfWork.Repository<Elevator>().Update(Item);
            this.SaveChanges();
            return updated;
        }

        // Save Changes is intentionally left to be called by controllers. 
        // If a user want sot save/update/delete data in different services, this will ensure transactional query through all services
        public bool SaveChanges()
        {
            unitOfWork.SaveChanges();
            return true;
        }
    }
}
