﻿using Exercise.Models;
using Exercise.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise.Services
{
    public class StatusService : IStatusService
    {
        private readonly IUnitOfWork unitOfWork;

        public StatusService(IUnitOfWork unitOfwork)
        {
            unitOfWork = unitOfwork;
        }

        public bool Delete(Status item)
        {
            unitOfWork.Repository<Status>().Delete(item);
            this.SaveChanges();
            return true;
        }

        public IEnumerable<Status> GetAll()
        {
            return unitOfWork.Repository<Status>().Get();
        }

        public Status GetById(Guid Id)
        {
            return this.unitOfWork.Repository<Status>().GetById(Id);
        }

        public Status Insert(Status Item, string[] include = null)
        {
            var addedItem = unitOfWork.Repository<Status>().Insert(Item);
            this.SaveChanges();
            if (include != null)
            {
                addedItem = unitOfWork.Repository<Status>().Get(x => x.Id == addedItem.Id, null, include).FirstOrDefault();
            }
            return addedItem;
        }

        public bool InsertRange(List<Status> Items)
        {
            foreach (var item in Items)
            {
                unitOfWork.Repository<Status>().Insert(item);
            }
            this.SaveChanges();
            return true;
        }

        public bool Update(Status Item)
        {
            var updated = this.unitOfWork.Repository<Status>().Update(Item);
            this.SaveChanges();
            return updated;
        }

        // Save Changes is intentionally left to be called by controllers. 
        // If a user want sot save/update/delete data in different services, this will ensure transactional query through all services
        public bool SaveChanges()
        {
            unitOfWork.SaveChanges();
            return true;
        }
    }
}
