﻿using Exercise.Models;
using System;
using System.Collections.Generic;

namespace Exercise.Services
{
    public interface IStatusService
    {
        Status GetById(Guid Id);
        bool InsertRange(List<Status> Items);
        Status Insert(Status Item, string[] include = null);
        bool Update(Status Item);
        IEnumerable<Status> GetAll();
        bool Delete(Status item);
    }
}
