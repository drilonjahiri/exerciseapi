﻿using Exercise.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercise.Services
{
    public interface IElevatorService
    {
        Elevator GetById(Guid Id);
        bool InsertRange(List<Elevator> Items);
        Elevator Insert(Elevator Item, string[] include = null);
        bool Update(Elevator Item);
        IEnumerable<Elevator> GetAll();
        IEnumerable<Elevator> GetAllByAccount(string username); 
        bool Delete(Elevator item);
    }
}
