﻿using Exercise.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Exercise.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly IDbContext context;
        internal DbSet<TEntity> dbSet;

        public Repository(IDbContext context)
        {
            this.context = context;
            this.dbSet = this.context.Set<TEntity>();
        }

        public TEntity GetById(Guid id, bool? includeDeletedEntities = null)
        {
            var entity = this.dbSet.Find(id);

            if ((includeDeletedEntities.HasValue && includeDeletedEntities == false) && entity != null && entity.IsDeleted)
            {
                return null;
            }
            else
            {
                return entity;
            }
        }

        public TEntity Insert(TEntity item)
        {
            return this.dbSet.Add(item).Entity;
        }

        public bool InsertRange(List<TEntity> items)
        {
            this.dbSet.AddRange(items);
            return true;
        }

        public bool Update(TEntity item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Item is null");
            }

            var entity = context.GetEntry(item);

            if (entity.State == EntityState.Detached)
            {
                this.dbSet.Attach(item);
            }

            entity.State = EntityState.Modified;

            if (item is BaseEntity)
            {
                entity.Property("DateCreated").IsModified = false;
                entity.Property("CreateBy").IsModified = false;
            }

            return true;
        }

        public bool Delete(TEntity item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("Item is null");
            }

            item.IsDeleted = true;
            item.DeletedDate = DateTime.Now;

            return true;
        }

        public bool DeleteRange(List<TEntity> items)
        {
            foreach (var item in items)
            {
                var rez = this.dbSet.Find(item);
                var entity = rez as EntityEntry;
                entity.State = EntityState.Deleted;

                if (entity.Entity != null && entity.Entity is BaseEntity && !(entity.Entity as BaseEntity).IsDeleted)
                {
                    (entity.Entity as BaseEntity).IsDeleted = true;
                    (entity.Entity as BaseEntity).DeletedDate = DateTime.Now;
                    entity.State = EntityState.Modified;
                }
            }

            return true;
        }

        public IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>,
            IOrderedQueryable<TEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null,
            bool? includeDeletedEntities = null)
        {
            IQueryable<TEntity> query = this.dbSet;

            if (!includeDeletedEntities.HasValue || includeDeletedEntities == false)
            {
                query = query.Where(x => !x.IsDeleted);
            }

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
            {
                includeProperties.ToList().ForEach(property =>
                {
                    query = query.Include(property);
                });
            }
            var returnobj = orderBy != null ? orderBy(query).ToList() : query.ToList();

            return orderBy != null ? orderBy(query).ToList() : query.ToList();
        }

        public TEntity GetSingle(
          Expression<Func<TEntity, bool>> filter = null,
          Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
          IEnumerable<string> includeProperties = null, bool? includeDeletedEntities = null)
        {
            IQueryable<TEntity> query = this.dbSet;

            if (!includeDeletedEntities.HasValue || includeDeletedEntities == false)
            {
                query = query.Where(x => !x.IsDeleted);
            }

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
            {
                includeProperties.ToList().ForEach(property =>
                {
                    query = query.Include(property);
                });
            }

            return orderBy != null ? orderBy(query).FirstOrDefault() : query.FirstOrDefault();
        }

        public IQueryable<TEntity> GetAsQueryable(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null, bool? includeDeletedEntities = null)
        {
            IQueryable<TEntity> query = this.dbSet;

            if (!includeDeletedEntities.HasValue || includeDeletedEntities == false)
            {
                query = query.Where(x => !x.IsDeleted);
            }

            var softDelete = this.dbSet as EntityEntry;

            if (filter != null)
            {
                query = query.Where(filter);
            }
            if (includeProperties != null)
            {
                includeProperties.ToList().ForEach(property => query = query.Include(property));
            }

            return query;
        }

        // Use GetAsNoTracking when you do not plan to update those entities
        public IEnumerable<TEntity> GetAsNoTracking(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null, bool? includeDeletedEntities = null)
        {
            IQueryable<TEntity> query = this.dbSet;

            if (!includeDeletedEntities.HasValue || includeDeletedEntities == false)
            {
                query = query.Where(x => !x.IsDeleted);
            }

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
            {
                includeProperties.ToList().ForEach(property => query = query.Include(property));
            }

            return orderBy != null ? orderBy(query).AsNoTracking().ToList() : query.AsNoTracking().ToList();
        }

        public bool Any(Expression<Func<TEntity, bool>> filter = null, bool? includeDeletedEntities = null)
        {
            IQueryable<TEntity> query = this.dbSet;

            if (!includeDeletedEntities.HasValue || includeDeletedEntities == false)
            {
                query = query.Where(x => !x.IsDeleted);
            }

            if (filter != null)
            {
                return query.Any(filter);
            }

            return query.Any();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public int GetCount()
        {
            IQueryable<TEntity> query = this.dbSet.Where(x => !x.IsDeleted);

            return query.Count();
        }
    }
}
