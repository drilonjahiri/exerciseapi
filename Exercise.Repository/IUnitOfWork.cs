﻿using Exercise.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercise.Repository
{
    public interface IUnitOfWork
    {
        IRepository<T> Repository<T>() where T : BaseEntity;
        void SaveChanges();
    }
}
