﻿using Exercise.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise.Repository
{
    public class AppContext : IdentityDbContext<
        AppUser,
        Role,
        Guid,
        IdentityUserClaim<Guid>,
        UserRole,
        IdentityUserLogin<Guid>,
        IdentityRoleClaim<Guid>,
        IdentityUserToken<Guid>
        >, IDbContext
    {
        public AppContext(DbContextOptions<AppContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Elevator> Elevators { get; set; }
        public DbSet<Status> Statuses { get; set; }

        public override int SaveChanges()
        {
            var entities = ChangeTracker.Entries<BaseEntity>()
              .Where(entity => entity.State == EntityState.Added || entity.State == EntityState.Modified);

            var now = DateTime.Now;
            var username = "";

            foreach (var entry in entities.Where(p => p.State == EntityState.Added))
            {
                entry.Entity.DateCreated = now;
                entry.Entity.CreateBy = username;
            }
            foreach (var entry in entities.Where(p => p.State == EntityState.Modified))
            {
                entry.Entity.LastUpdatedDate = now;
                entry.Entity.LastUpdateBy = username;
            }

            return base.SaveChanges();
        }

        DbSet<T> IDbContext.Set<T>()
        {
            return this.Set<T>();
        }

        public EntityEntry GetEntry(object item)
        {
            return this.Entry(item);
        }

        public void Rollback()
        {
            this.ChangeTracker.Entries().ToList().ForEach(x => x.Reload());
        }
    }
}
