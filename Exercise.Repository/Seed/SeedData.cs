﻿using Exercise.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise.Repository.Seed
{
    public class SeedData
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly AppContext _dataContext;
        private readonly RoleManager<Role> _roleManager;

        public SeedData(UserManager<AppUser> userManager, AppContext dataContext, RoleManager<Role> roleManager)
        {
            _userManager = userManager;
            _dataContext = dataContext;
            _roleManager = roleManager;
        }

        public void Init()
        {
            if (!_userManager.Users.Any())
            {
                var roles = Enum.GetValues(typeof(Models.Enums.Roles));
                foreach (var item in roles)
                {
                    _roleManager.CreateAsync(new Role { Name = item.ToString() }).Wait();
                }

                var superAdmin = new AppUser
                {
                    Id = Guid.Parse("FD81B50A-EC12-4C5F-BD9C-08D6788A27F0"),
                    UserName = "superadmin",
                    Name = "Super",
                    LastName = "Admin",
                    Email = "super@admin.com"
                };
                _userManager.CreateAsync(superAdmin, "password").Wait();
                _userManager.AddToRoleAsync(superAdmin, "SuperAdmin").Wait();

                var johnWick = new AppUser
                {
                    Id = Guid.Parse("CE66F700-C580-4E86-7E2B-08D6774F9B60"),
                    UserName = "johnw",
                    Name = "John",
                    LastName = "Wick",
                    Email = "john@email.com"
                };
                _userManager.CreateAsync(johnWick, "password").Wait();
                _userManager.AddToRoleAsync(johnWick, "SuperAdmin").Wait();

                var testUser = new AppUser
                {
                    Id = Guid.Parse("82BD3265-BD4A-4B99-672D-08D66DBD3EEE"),
                    UserName = "test",
                    Name = "test",
                    LastName = "test user",
                    Email = "test@email.com"
                };

                _userManager.CreateAsync(testUser, "password").Wait();
                _userManager.AddToRoleAsync(testUser, "SuperAdmin").Wait();
            }

            if (!_dataContext.Elevators.Any())
            {
                SeedOtherData();
            }
        }

        private void SeedOtherData()
        {
            Status s = new Status();
            s.Id = Guid.Parse("A0B748AF-1595-4822-D991-08D678CCF13B");
            s.DateCreated = DateTime.Now;
            s.Name = "Working";

            Status s1 = new Status();
            s1.Id = Guid.Parse("0207C05B-D610-4A48-D992-08D678CCF13B");
            s1.DateCreated = DateTime.Now;
            s1.Name = "Damaged";

            Status s2 = new Status();
            s2.Id = Guid.Parse("47B09E45-B271-45C2-D993-08D678CCF13B");
            s2.DateCreated = DateTime.Now;
            s2.Name = "Testing";

            Status s3 = new Status();
            s3.Id = Guid.Parse("C008FD53-9CE8-4A61-D994-08D678CCF13B");
            s3.DateCreated = DateTime.Now;
            s3.Name = "Offline";

            Status s4 = new Status();
            s4.Id = Guid.Parse("155A3DF7-9285-43F1-D995-08D678CCF13B");
            s4.DateCreated = DateTime.Now;
            s4.Name = "SentToMaintentance";

            _dataContext.Statuses.Add(s);
            _dataContext.Statuses.Add(s1);
            _dataContext.Statuses.Add(s2);
            _dataContext.Statuses.Add(s3);
            _dataContext.Statuses.Add(s4);


            Elevator a = new Elevator();
            a.DateCreated = DateTime.Now;
            a.StatusId = Guid.Parse("A0B748AF-1595-4822-D991-08D678CCF13B");
            a.UserId = Guid.Parse("FD81B50A-EC12-4C5F-BD9C-08D6788A27F0");
            a.LastUpdatedDate = DateTime.Now;

            Elevator b = new Elevator();
            b.DateCreated = DateTime.Now;
            b.StatusId = Guid.Parse("0207C05B-D610-4A48-D992-08D678CCF13B");
            b.UserId = Guid.Parse("FD81B50A-EC12-4C5F-BD9C-08D6788A27F0");
            b.LastUpdatedDate = DateTime.Now;

            Elevator c = new Elevator();
            c.DateCreated = DateTime.Now;
            c.StatusId = Guid.Parse("47B09E45-B271-45C2-D993-08D678CCF13B");
            c.UserId = Guid.Parse("82BD3265-BD4A-4B99-672D-08D66DBD3EEE");
            c.LastUpdatedDate = DateTime.Now;

            Elevator d = new Elevator();
            d.DateCreated = DateTime.Now;
            d.StatusId = Guid.Parse("A0B748AF-1595-4822-D991-08D678CCF13B");
            d.UserId = Guid.Parse("FD81B50A-EC12-4C5F-BD9C-08D6788A27F0");
            d.LastUpdatedDate = DateTime.Now;

            Elevator e = new Elevator();
            e.DateCreated = DateTime.Now;
            e.StatusId = Guid.Parse("A0B748AF-1595-4822-D991-08D678CCF13B");
            e.UserId = Guid.Parse("CE66F700-C580-4E86-7E2B-08D6774F9B60");
            e.LastUpdatedDate = DateTime.Now;

            Elevator f = new Elevator();
            f.DateCreated = DateTime.Now;
            f.StatusId = Guid.Parse("A0B748AF-1595-4822-D991-08D678CCF13B");
            f.UserId = Guid.Parse("82BD3265-BD4A-4B99-672D-08D66DBD3EEE");
            f.LastUpdatedDate = DateTime.Now;

            Elevator g = new Elevator();
            g.DateCreated = DateTime.Now;
            g.StatusId = Guid.Parse("A0B748AF-1595-4822-D991-08D678CCF13B");
            g.UserId = Guid.Parse("FD81B50A-EC12-4C5F-BD9C-08D6788A27F0");
            g.LastUpdatedDate = DateTime.Now;

            Elevator h = new Elevator();
            h.DateCreated = DateTime.Now;
            h.StatusId = Guid.Parse("0207C05B-D610-4A48-D992-08D678CCF13B");
            h.UserId = Guid.Parse("CE66F700-C580-4E86-7E2B-08D6774F9B60");
            h.LastUpdatedDate = DateTime.Now;

            Elevator i = new Elevator();
            i.DateCreated = DateTime.Now;
            i.StatusId = Guid.Parse("0207C05B-D610-4A48-D992-08D678CCF13B");
            i.UserId = Guid.Parse("82BD3265-BD4A-4B99-672D-08D66DBD3EEE");
            i.LastUpdatedDate = DateTime.Now;

            Elevator j = new Elevator();
            j.DateCreated = DateTime.Now;
            j.StatusId = Guid.Parse("A0B748AF-1595-4822-D991-08D678CCF13B");
            j.UserId = Guid.Parse("FD81B50A-EC12-4C5F-BD9C-08D6788A27F0");
            j.LastUpdatedDate = DateTime.Now;

            Elevator k = new Elevator();
            k.DateCreated = DateTime.Now;
            k.StatusId = Guid.Parse("C008FD53-9CE8-4A61-D994-08D678CCF13B");
            k.UserId = Guid.Parse("CE66F700-C580-4E86-7E2B-08D6774F9B60");
            k.LastUpdatedDate = DateTime.Now;

            Elevator l = new Elevator();
            l.DateCreated = DateTime.Now;
            l.StatusId = Guid.Parse("A0B748AF-1595-4822-D991-08D678CCF13B");
            l.UserId = Guid.Parse("FD81B50A-EC12-4C5F-BD9C-08D6788A27F0");
            l.LastUpdatedDate = DateTime.Now;

            Elevator ll = new Elevator();
            ll.DateCreated = DateTime.Now;
            ll.StatusId = Guid.Parse("C008FD53-9CE8-4A61-D994-08D678CCF13B");
            ll.UserId = Guid.Parse("CE66F700-C580-4E86-7E2B-08D6774F9B60");
            ll.LastUpdatedDate = DateTime.Now;

            _dataContext.Elevators.Add(a);
            _dataContext.Elevators.Add(b);
            _dataContext.Elevators.Add(c);
            _dataContext.Elevators.Add(d);
            _dataContext.Elevators.Add(e);
            _dataContext.Elevators.Add(f);
            _dataContext.Elevators.Add(g);
            _dataContext.Elevators.Add(h);
            _dataContext.Elevators.Add(i);
            _dataContext.Elevators.Add(j);
            _dataContext.Elevators.Add(k);
            _dataContext.Elevators.Add(l);
            _dataContext.Elevators.Add(ll);
            _dataContext.SaveChanges();
        }
    }
}
