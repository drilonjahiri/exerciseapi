﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Exercise.Repository.Migrations
{
    public partial class ElevatorforOneUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                table: "Elevators",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Elevators_UserId",
                table: "Elevators",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Elevators_AspNetUsers_UserId",
                table: "Elevators",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Elevators_AspNetUsers_UserId",
                table: "Elevators");

            migrationBuilder.DropIndex(
                name: "IX_Elevators_UserId",
                table: "Elevators");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Elevators");
        }
    }
}
