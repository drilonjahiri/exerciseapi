﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Exercise.Repository.Migrations
{
    public partial class LastUpdatedTimeStamp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "LastUpdated",
                table: "Elevators",
                rowVersion: true,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastUpdated",
                table: "Elevators");
        }
    }
}
