﻿using Exercise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Exercise.Repository
{
    public interface IRepository<TEntity> : IDisposable where TEntity : BaseEntity
    {
        TEntity GetById(Guid id, bool? includeDeletedEntities = null);

        TEntity GetSingle(Expression<Func<TEntity, bool>> filter = null,
                Func<IQueryable<TEntity>, 
                IOrderedQueryable<TEntity>> orderBy = null,
                IEnumerable<string> includeProperties = null, 
                bool? includeDeletedEntities = null);

        int GetCount();

        TEntity Insert(TEntity entity);

        bool InsertRange(List<TEntity> items);

        bool Update(TEntity entity);

        bool Delete(TEntity item);
        bool DeleteRange(List<TEntity> items);

        IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>,
            IOrderedQueryable<TEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null,
            bool? includeDeletedEntities = null);

        IEnumerable<TEntity> GetAsNoTracking(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>,
            IOrderedQueryable<TEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null,
            bool? includeDeletedEntities = null);

        IQueryable<TEntity> GetAsQueryable(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>,
            IOrderedQueryable<TEntity>> orderBy = null,
            IEnumerable<string> includeProperties = null,
            bool? includeDeletedEntities = null);

        bool Any(Expression<Func<TEntity, bool>> filter = null, 
                bool? includeDeletedEntities = null);
    }
}
