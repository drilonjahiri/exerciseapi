﻿using Exercise.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exercise.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private dynamic tRepository;
        private readonly IDbContext dbContext;
        private readonly Dictionary<Type, object> _repositories = new Dictionary<Type, object>();

        public UnitOfWork(IDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public Dictionary<Type, object> Repositories
        {
            get { return _repositories; }
            set { Repositories = value; }
        }

        // Unit of work is singeltone per request. This ensures that the appropriate repository is returned for approrpriate entity
        // Changes should be transactional 
        public IRepository<T> Repository<T>() where T : BaseEntity
        {
            if (Repositories.Keys.Contains(typeof(T)))
            {
                return Repositories[typeof(T)] as IRepository<T>;
            }

            IRepository<T> repo = new Repository<T>(dbContext);
            Repositories.Add(typeof(T), repo);
            return repo;
        }

        public void Rollback()
        {
            this.Rollback();
        }

        public void SaveChanges()
        {
            this.dbContext.SaveChanges();
        }

        public void Dispose()
        {
            this.dbContext.Dispose();
        }
    }
}
