﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercise.Repository
{
    public interface IDbContext
    {
        DbSet<T> Set<T>() where T : class;
        EntityEntry GetEntry(object item);
        int SaveChanges();
        void Dispose();
        void Rollback();
    }
}
